package makkoi.DoujinsakkaWiki;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoujinsakkaWikiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoujinsakkaWikiApplication.class, args);
	}

}
